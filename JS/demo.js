"use strict";
var g = {};

function createSection(title, description, highlight) {
  var li = document.createElement("li");
  if (highlight){
    var star = document.createElement("img");
    star.setAttribute("src", "images/star-128.png");
    star.setAttribute("alt", "star");
    li.appendChild(star);
  }
  var a = document.createElement("a");
  a.href = "#";
  var h3 = document.createElement("h3");
  h3.textContent = title;
  a.appendChild(h3);
  li.appendChild(a);
  var p = document.createElement("p");
  p.textContent = description;
  li.appendChild(p);
  return li;
}

function displayContent(){
  if (g.requestyaml.readyState === 4 && g.requestyaml.status === 200) {
    var doc;
    try {
      doc = jsyaml.load(g.requestyaml.responseText);
    } catch (e) {
      console.log(e);
    }
    var section1 = document.getElementById("section1");

    var section1Content = doc["The big index"].Robots;
    var h2 = document.createElement("h2");
    h2.textContent = "Robots";
    section1.appendChild(h2);
    var ul = document.createElement("ul");
    section1Content.forEach(function(element){
      var il = createSection(element.title, element.description, element.highlight);
      ul.appendChild(il);
    })
    section1.appendChild(ul);
    h2 = document.createElement("h2");
    h2.textContent = "Sensors";
    var section2 = document.getElementById("section2");
    var section2Content = doc["The big index"].Sensors;
    section2.appendChild(h2);

    ul = document.createElement("ul");
    section2Content.forEach(function(element){
      var il = createSection(element.title, element.description, element.highlight);
      ul.appendChild(il);
    })
    section2.appendChild(ul);
  }
}

function createRequestObject() {
  var request;
  if (window.XMLHttpRequest) {
    request = new XMLHttpRequest();
  } else {
    request = new ActiveXObject("Microsoft.XMLHTTP");
  }
  return request;
}

function setUp() {
  g.requestyaml = createRequestObject();
  // designate a handler to be called when the request completes
  g.requestyaml.onreadystatechange = function () {
    displayContent();
  };
  // request the specific language text file
  g.requestyaml.open("GET", "bigindex.yaml", true);
  // send the request asynchronously
  // If the server takes a long time to respond, other events will continue to be processed
  // until 'readystatechange' is triggered (server has responded) and processed
  g.requestyaml.send(null);
}

document.addEventListener("DOMContentLoaded", function () {
  // Get document, or throw exception on error
  setUp();


});
