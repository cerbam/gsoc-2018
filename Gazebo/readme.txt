1)Install the gazebo simulator, create a robot model of your own (or modify an existing model) and make it move. If it is your first experience, write down the different difficulties you face in the process.
It is my first experience with Gazebo. I decided to use Turtlebot2 to explore Gazebo capabilities.
As OS I decided to use Ubuntu  16.04 Xenial
NEX STEP: Instaling Gazebo9. Was installed without any problems.
NEX STEP: Instaling ROS. I decided to use Indigo because Turtlebot2 doesn't support Kinetic and Lunar.
First problem:  Can't install ros-indigo-desktop-full on Ubuntu  16.04(ros-indigo-desktop-full package doesn't support on Ubuntu  16.04)
Decision: Install Kinetic
Next problem: By documentation turtlebot2 doesn't support ros-kinetic (http://robots.ros.org/)
Decision: Use Kinetic to check if it is true.
Next problem: Can't find documentation for installing turtlebot using kinetic (http://wiki.ros.org/turtlebot/Tutorials)
Decision: Use indigo tutorial as an example. I did the same steps but using kinetic instead of indigo
Finally: I have run turtlebot2 using Ubuntu 16.04 + gazebo 9 + ros-kinetic.
